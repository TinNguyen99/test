
import App from '../App';
import React  from 'react';
// import 'jsdom-global/register';
import Enzyme from "enzyme";
import {act, renderHook} from '@testing-library/react-hooks'
// import { render, fireEvent, getByTestId} from "@testing-library/react";
// import ReactDOM from "react-dom";
import { configure, shallow, mount } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';

configure({ adapter: new Adapter() });

// mock module TouchableOpacity to Jest testing
jest.mock(
  'react-native/Libraries/Components/Touchable/TouchableOpacity.js',
  () => {
    const { TouchableHighlight } = require('react-native')
    const MockTouchable = props => {
      return <TouchableHighlight {...props} />
    }
    MockTouchable.displayName = 'TouchableOpacity'

    return MockTouchable
  }
)

// Test body
describe('App Component', () => {
  it('renders <App /> components', () => {
    const wrapper = shallow(<App />);

    for(let i = 1; i <= 5; ++i){
      wrapper.find('TouchableOpacity').simulate('press');
      expect(wrapper.find('Text').at(0).childAt(0).text()).toEqual(i.toString());
    }
  });

  it('Should have Add string', () => {
    const wrapper = shallow(<App />);
      expect(wrapper.find('Text').at(1).childAt(0).text()).toEqual('Add +');
  });
});