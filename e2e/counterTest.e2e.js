describe('Test Counter Mode', () => {

  const waitFor = (duration) =>
    new Promise((resolve) => setTimeout(() => resolve(), duration));


  beforeAll(async () => {
    await device.launchApp();
  });

  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should count correctly', async () => {

    for (let i = 1; i <= 10; ++i){
      await element(by.id('button')).tap();
      await waitFor(300);
      
      await expect(element(by.id('count'))).toHaveText(`${i}`);
    }
  });
});





