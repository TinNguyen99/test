
// import 'jsdom-global/register';
import React , {useState} from 'react';

import {
  Button,
  FlatList,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';


const App = () => {
  const [txt, setTxt] = useState(0);
  // const [t1, setT1] = useState(1999);
  // const [t2, setT2] = useState(2020);
  // const [t3, setT3] = useState(2021);

  const add = () => {
    setTxt(txt+1)
  }

  return (
  <View style={styles.container}>
    <Text testID='count'  style={styles.count}>{txt}</Text>
    <TouchableOpacity testID='button' style={styles.btnStyle} onPress={()=>add()}>
      <Text style={styles.txt}>Add +</Text>
    </TouchableOpacity>

    {/* <Text style={styles.txt}>{t1}</Text>
    <Text style={styles.txt}>{t2}</Text>
    <Text style={styles.txt}>{t3}</Text> */}

  </View>  
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnStyle: {
    alignSelf: 'center', 
    marginVertical: 10, 
    backgroundColor: 'lightblue',
    justifyContent: 'center',
    alignItems: 'center',
    width: 200,
    height: 70,
    borderRadius: 10,
  },
  txt: {
    fontSize: 18,
    fontWeight: 'bold',
  },
  count: {
    alignSelf: 'center', 
    fontSize: 40, 
    marginVertical: 10, 
  },
});

export default App;
